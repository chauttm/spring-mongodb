package hellomongo.controller;

import hellomongo.domain.Comment;
import hellomongo.domain.Product;
import hellomongo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dse on 19/03/2016.
 */
@Controller
public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @RequestMapping("/")
    public String home() {
        return "home";
    }

    @RequestMapping("/products")
    public String index(Model model)
    {
        List<Product> products = productRepository.findAll();
        model.addAttribute("products", products);
        return "products";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/products/new", method= RequestMethod.GET)
    public String add(Model model)
    {
        model.addAttribute("product", new Product());
        return "productNew";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/products/new", method=RequestMethod.POST)
    public String add(@ModelAttribute Product product, @RequestParam(value="id") String id,
            Model model)
    {
        if (id == null || id.isEmpty())
            productRepository.save(product);
        else {
            Product oldProduct = productRepository.findOne(id);
            oldProduct.setTitle(product.getTitle());
            oldProduct.setDescription(product.getDescription());
            oldProduct.setUrl(product.getUrl());
            productRepository.save(oldProduct);
        }
        model.addAttribute("product", product);
        return "redirect:/products";
    }

    @RequestMapping(value = "/products/comment", method=RequestMethod.POST)
    public String comment(@RequestParam(value="id") String id,
                          @RequestParam(value="message") String message,
                          Principal principal,
                          Model model)
    {
        Product product = productRepository.findOne(id);
        if (product.getComments() == null) product.setComments(new ArrayList<>());
        product.getComments().add(new Comment(principal.getName(), message));
        productRepository.save(product);
        model.addAttribute("product", product);
        return "product";
    }

    @RequestMapping(value = "/products/show")
    public String show(
            @RequestParam(value="id") String id, Model model)
    {
        Product product = productRepository.findOne(id);
        if (product != null) {
            model.addAttribute("product", product);
        }
        return "product";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/products/edit")
    public String edit(
            @RequestParam(value="id") String id, Model model)
    {
        Product product = productRepository.findOne(id);
        if (product != null) {
            model.addAttribute("product", product);
        }
        return "productNew";
    }

    @Secured({"ROLE_ADMIN"})
    @RequestMapping(value = "/products/delete", method = RequestMethod.POST)
    public String delete(
            @RequestParam(value="id") String id)
    {
        Product product = productRepository.findOne(id);
        if (product != null)
            productRepository.delete(product);
        return "redirect:/products";
    }
}