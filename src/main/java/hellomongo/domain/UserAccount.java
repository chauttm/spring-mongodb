package hellomongo.domain;

import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dse on 26/03/2016.
 */
public class UserAccount {

    @Id
    private String username;

    private String password;

    private List<GrantedAuthority> roles;

    public UserAccount(String username, String password) {
        this.username = username;
        this.password = password;
        this.roles = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<GrantedAuthority> getRoles() {
        return roles;
    }
}
