package hellomongo.domain;

import java.util.Date;

/**
 * Created by dse on 19/03/2016.
 */
public class Comment {
    String by;
    String message;
    Date dateCreated;
    int likes;

    public Comment(String by, String message) {
        this.by = by;
        this.message = message;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
