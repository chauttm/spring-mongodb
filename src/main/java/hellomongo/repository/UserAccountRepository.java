package hellomongo.repository;

import hellomongo.domain.UserAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by dse on 26/03/2016.
 */
public interface UserAccountRepository extends MongoRepository<UserAccount, String> {
}
