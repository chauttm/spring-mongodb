package hellomongo.security;

import hellomongo.domain.UserAccount;
import hellomongo.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Created by dse on 26/03/2016.
 */
@Service
public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    @Autowired
    UserAccountRepository userAccountRepository;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        userAccountRepository.deleteAll();
        UserAccount alice = new UserAccount("alice", "12345");
        alice.getRoles().add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        userAccountRepository.save(alice);
        UserAccount bob = new UserAccount("Bob", "12345");
        userAccountRepository.save(bob);

        UserAccount user = userAccountRepository.findOne(username);
        if (user == null || !user.getPassword().equals(authentication.getCredentials()))
            throw new BadCredentialsException("Invalid username or password");
        return new User(user.getUsername(), user.getPassword(), user.getRoles());
    }
}
